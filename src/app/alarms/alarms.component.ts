import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Alarm } from '../Model/Alarm';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alarms',
  templateUrl: './alarms.component.html',
  styleUrls: ['./alarms.component.css']
})
export class AlarmsComponent implements OnInit {

  endpoint = 'https://05r16bznx2.execute-api.eu-west-1.amazonaws.com/3/rest/alarms';
  httpOption = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };
  alarms: Alarm[];
  selectedAlarm: Alarm;

  constructor(private http: HttpClient, private route: Router) { }

  ngOnInit() {
    this.getAllAlarms();
  }

  goto() {
    this.route.navigate(['/detail']);
  }

  onSelect(alarm: Alarm): void {
    this.selectedAlarm = alarm;
  }

  getAlarms(): Observable<Alarm[]> {
    console.log('getting all alarms from the server');
    return this.http.get<Alarm[]>(this.endpoint, this.httpOption);
  }

  getAllAlarms() {
    this.getAlarms()
      .subscribe(
        (data: Alarm[]) => { 
          this.alarms = data;
          console.log(this.alarms);
        }, 
        (error: any) => console.log(error),
        () => console.log('all data gets')
      );
  }
}
