export class Alarm {
    name: string;
    refreshRateInMs: number;
    status: string;
}
