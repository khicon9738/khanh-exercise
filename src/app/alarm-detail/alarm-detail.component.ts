import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Alarm } from '../Model/Alarm';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-alarm-detail',
  templateUrl: './alarm-detail.component.html',
  styleUrls: ['./alarm-detail.component.css']
})
export class AlarmDetailComponent implements OnInit {

  endpoint = 'https://05r16bznx2.execute-api.eu-west-1.amazonaws.com/3/rest/alarms';
  alarm: Alarm;
  subscription: Subscription;

  constructor(
    private router: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getAlarm();
  }

  getAlarm() {
    this.http.get(this.endpoint + '/' + this.router.snapshot.paramMap.get('id'))
      .subscribe(
        (data: Alarm) => {
          this.alarm = data;
          setInterval(() => location.reload(), this.alarm.refreshRateInMs);
        },
        (error: any) => console.log(error),
        () => console.log('Get info success')
      );
  }
}
